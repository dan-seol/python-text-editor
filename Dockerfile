FROM continuumio/miniconda3

WORKDIR /src/python-text-editor

COPY environment.yml /src/python-text-editor/

RUN conda install -c conda-forge gcc python=3.12 \
    && conda env update -n base -f environment.yml

COPY . /src/python-text-editor

RUN pip install --no-deps -e .
