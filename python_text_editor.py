from functools import partial
from tkinter import Text, Tk, filedialog, ttk

__version__ = "0.0.1"

TEXT_INDEX = "1.0"
END_1C = "end-1c"
TEXT_EDITOR_CLASS_NAME = " Text Editor"
TEXT_EDITOR_LABEL = "Type Something!"
SAVE = "Save"
QUIT = "Quit"


def get_frame(root) -> ttk.Frame:
    """Create a frame on the grid and returns it."""
    frame = ttk.Frame(root, padding=10)
    frame.grid()
    return frame


def get_text(root) -> Text:
    """Create a text type box and returns it."""
    text = Text(root)
    return text


def save_as(text: Text):
    """Save the text file content."""
    text_content = text.get(TEXT_INDEX, END_1C)
    save_location = filedialog.asksaveasfilename()
    with open(save_location, "w+") as file_to_save:
        file_to_save.write(text_content)


def run():
    root = Tk(className=TEXT_EDITOR_CLASS_NAME)
    text = get_text(root)
    frame = get_frame(root)
    ttk.Label(frame, text=TEXT_EDITOR_LABEL).grid(column=0, row=0)
    ttk.Button(frame, text=SAVE, command=partial(save_as, text)).grid(column=1, row=1)
    ttk.Button(frame, text=QUIT, command=root.destroy).grid(column=2, row=1)
    text.grid()
    root.mainloop()


if __name__ == "__main__":
    run()
