# Welcome to python_text_editor's documentation!

a simple text editor created in python.

```{toctree}
:caption: 'Contents:'
:maxdepth: 2

API Reference <_api/python_text_editor/index>
```

# Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
